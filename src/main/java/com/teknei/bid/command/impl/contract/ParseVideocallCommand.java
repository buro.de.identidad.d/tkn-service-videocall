package com.teknei.bid.command.impl.contract;

import com.teknei.bid.command.Command;
import com.teknei.bid.command.CommandRequest;
import com.teknei.bid.command.CommandResponse;
import com.teknei.bid.command.Status;
import com.teknei.bid.service.remote.QuobisServiceRemote;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class ParseVideocallCommand implements Command {

    private static final Logger log = LoggerFactory.getLogger(ParseVideocallCommand.class);

    @Autowired
    private QuobisServiceRemote quobisServiceRemote;

    @Override
    public CommandResponse execute(CommandRequest request) {
        CommandResponse response = new CommandResponse();
        response.setId(request.getId());
        try {
            byte[] calleeData = quobisServiceRemote.getCallee(request.getId());
            byte[] callerData = quobisServiceRemote.getCaller(request.getId());
            byte[] selfieData = quobisServiceRemote.getSelfie(request.getId());
            response.setCallee(calleeData);
            response.setCaller(callerData);
            response.setSelfie(selfieData);
            response.setStatus(Status.VIDEOCALL_QUOBIS_OK);
        } catch (Exception e) {
            log.error("Erorr in parseVideocallCommand for request: {} with message: {}", request, e.getMessage());
            response.setStatus(Status.VIDEOCALL_QUOBIS_ERROR);
        }
        return response;
    }


}
