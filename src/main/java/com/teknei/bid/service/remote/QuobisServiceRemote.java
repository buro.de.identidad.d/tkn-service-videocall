package com.teknei.bid.service.remote;

import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

//MCG @FeignClient(name = "QUOBIS-CLIENT", url = "${tkn.quobis.url}")
@FeignClient(name = "QUOBIS-CLIENT", url = "${tkn.quobis.storage.url}")
public interface QuobisServiceRemote {

    @RequestMapping(value = "/{id}/selfie.png", method = RequestMethod.GET)
    byte[] getSelfie(@PathVariable("id") Long id);

    @RequestMapping(value = "/{id}/caller.webm", method = RequestMethod.GET)
    byte[] getCaller(@PathVariable("id") Long id);

    @RequestMapping(value = "/{id}/callee.webm", method = RequestMethod.GET)
    byte[] getCallee(@PathVariable("id") Long id);
}